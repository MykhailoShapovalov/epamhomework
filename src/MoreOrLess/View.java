package MoreOrLess;

import java.util.Scanner;
import java.util.StringJoiner;

public class View {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Controller controller = new Controller();
        Model model = new Model();
        System.out.println("Welcome to game 'More Or Less!'");

        System.out.println("Please, enter the minimum value for range or press enter:");
        String minStr = scanner.nextLine();
        controller.checkInputMinVal(minStr, model);
        System.out.println("Value of the minimum range is: " + model.getMin());

        System.out.println("Please, enter the maximum value for range or press enter:");
        String maxStr = scanner.nextLine();
        controller.checkInputMaxVal(maxStr, model.getMin(), model);
        System.out.println("Value of the maximum range is: " + model.getMax());

        System.out.println();
        model.rand(model.getMin(), model.getMax());
        System.out.println("I chose the number!");

        int playerNum = 0;
        int count = 0;
        StringJoiner stringJoiner = new StringJoiner(", ", "[", "]");
        while (model.getNum() != playerNum) {
            if (stringJoiner.toString().length() > 2) {
                System.out.println("Previous attempts: " + stringJoiner);
            }
            System.out.println("Please, enter a number belonging to the range: [" + model.getMin() + ", " + model.getMax() + "]");
            String strPlayerNum = scanner.nextLine();
            if (controller.belongingToRange(strPlayerNum, model.getMin(), model.getMax())) {
                count++;
                stringJoiner.add(strPlayerNum);
                playerNum = Integer.parseInt(strPlayerNum);
                if (controller.match(strPlayerNum, model.getNum())) {
                    System.out.println("Congratulations! You win! The number is: " + model.getNum());
                    System.out.println("Total count of attempts is: " + count + ", all attempts is: " + stringJoiner);
                } else {
                    System.out.println(controller.installNewRange(playerNum, model.getNum(), model));
                }
            } else {
                System.out.println("Hey! The number have to belong to the range: [" + model.getMin() + ", " + model.getMax() + "]!");
            }
        }
    }
}
