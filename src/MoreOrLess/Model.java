package MoreOrLess;

public class Model {

    private int min;
    private int max;
    private int num;
    public final int RAND_MIN = 0;
    public final int RAND_MAX = 100;

    protected void setMin(int min) {
        this.min = min;
    }

    protected void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    protected int getNum() {
        return num;
    }

    public int rand(int min, int max) {
        return num = (int) (min + Math.random() * (max - min));
    }

}
