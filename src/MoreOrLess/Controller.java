package MoreOrLess;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {

    private final Pattern pattern = Pattern.compile("[0-9]+");

    protected void checkInputMinVal(String minVal, Model model) {
        Matcher matcher = pattern.matcher(minVal);
        if (matcher.matches()) {
            int min = Integer.parseInt(minVal);
            if (min >= 0 && min <= 99) {
                model.setMin(min);
            } else {
                model.setMin(model.RAND_MIN);
            }
        } else {
            model.setMin(model.RAND_MIN);
        }
    }

    protected void checkInputMaxVal(String maxVal, int min, Model model) {
        Matcher matcher = pattern.matcher(maxVal);
        if (matcher.matches()) {
            int max = Integer.parseInt(maxVal);
            if (max <= 100 && max > min) {
                model.setMax(max);
            } else {
                model.setMax(model.RAND_MAX);
            }
        } else {
            model.setMax(model.RAND_MAX);
        }
    }

    protected boolean belongingToRange(String playerNum, int min, int max) {
        Matcher matcher = pattern.matcher(playerNum);
        if (matcher.matches()) {
            int num = Integer.parseInt(playerNum);
            return num >= min && num <= max;
        }
        return false;
    }

    protected boolean match(String playerNum, int num) {
        Matcher matcher = pattern.matcher(playerNum);
        if (matcher.matches()) {
            return Integer.parseInt(playerNum) == num;
        }
        return false;
    }

    protected String installNewRange(int playerNum, int num, Model model) {
        if (playerNum > num) {
            model.setMax(playerNum - 1);
            return "Less!";
        } else {
            model.setMin(playerNum + 1);
            return "More!";
        }
    }


}
