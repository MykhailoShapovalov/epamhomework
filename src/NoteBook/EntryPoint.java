package NoteBook;

import NoteBook.controller.Controller;

public class EntryPoint {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.start();
    }
}
