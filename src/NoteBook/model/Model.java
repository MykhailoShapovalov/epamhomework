package NoteBook.model;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {

    private final Map<String, String> noteBook = new HashMap<>();

    private final String lastNameRegex = "[a-zA-Z]+";
    private final String nickNameRegex = "[a-zA-Z]+";

    private final Pattern patternLastName = Pattern.compile(lastNameRegex);
    private final Pattern patternNickName = Pattern.compile(nickNameRegex);

    public String checkLastName(String lastName) {
        Matcher matcher = patternLastName.matcher(lastName);
        if (matcher.matches()) {
            return lastName;
        }
        return null;
    }

    public String checkNickName(String nickName) {
        Matcher matcher = patternNickName.matcher(nickName);
        if (matcher.matches()) {
            return nickName;
        }
        return null;
    }

    public void add(String lastName, String nickName) {
        if (lastName != null && nickName != null) {
            noteBook.put(lastName, nickName);
        }
    }

    public void getNoteBook() {
        noteBook.forEach((l, n) -> {
            System.out.println("Last name: " + l + ", Nickname: " + n + ".");
        });
    }
}
