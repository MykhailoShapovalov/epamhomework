package NoteBook.controller;

import NoteBook.model.Model;
import NoteBook.view.View;
import java.util.Scanner;

public class Controller {

    View view = new View();
    Model model = new Model();
    Scanner scanner;

    public void start() {
        view.showInfo(view.hello);
        boolean stop = false;
        String yes = "yes";
        while (!stop) {
            scanner = new Scanner(System.in);
            view.showInfo(view.writeLastName);
            String lastName = scanner.nextLine();
            while (model.checkLastName(lastName) == null) {
                view.showInfo(view.wrong);
                view.showInfo(view.writeLastName);
                lastName = scanner.nextLine();
            }
            view.showInfo(view.writeNickName);
            String nickName = scanner.nextLine();
            while (model.checkNickName(nickName) == null) {
                view.showInfo(view.wrong);
                view.showInfo(view.writeNickName);
                nickName = scanner.nextLine();
            }
            model.add(lastName, nickName);
            view.showInfo(view.writeOnceMore);
            if (!scanner.nextLine().toLowerCase().replaceAll(" ", "").equals(yes)) {
                stop = true;
            }
        }
        view.showInfo(view.showContact);
        if (scanner.nextLine().toLowerCase().replaceAll(" ", "").equals(yes)) {
            model.getNoteBook();
        }

    }
}
