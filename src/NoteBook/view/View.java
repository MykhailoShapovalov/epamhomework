package NoteBook.view;

public class View {

    public String hello = "Welcome to NoteBook!";

    public String writeLastName = "Please, write Last Name:";
    public String writeNickName = "Please, write Nickname:";

    public String wrong = "Wrong! This field can only contain capital or small letters.";

    public String writeOnceMore = "If you want to write once note more, please enter: yes, or press 'enter' for exit.";
    public String showContact = "If you want to see your notes, enter 'yes', or press 'enter' for exit.";

    public void showInfo(String message) {
        System.out.println(message);
    }
}
