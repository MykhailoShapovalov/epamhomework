package NoteBook2;

import NoteBook2.controller.Controller;
import NoteBook2.model.Model;
import NoteBook2.view.View;

public class EntryPoint {
    public static void main(String[] args) {
        Controller controller = new Controller(new Model(), new View());
        controller.start();
    }
}
