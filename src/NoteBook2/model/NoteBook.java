package NoteBook2.model;

public enum NoteBook {
    John("John", "jn101"), Boris("Boris", "boria3"), NICKOLAS("Nickolas", "nick21"),
    OLGA("Оля", "olia3"), Valentin("Валентин", "valia2");

    private final String name;
    private final String login;


    NoteBook(String name, String login) {
        this.name = name;
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public static boolean checkLogin(String login) {
        for (NoteBook note:NoteBook.values()) {
            if (note.getLogin().equals(login)) {
                return true;
            }
        }
        return false;
    }
}
