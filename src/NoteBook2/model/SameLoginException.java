package NoteBook2.model;

public class SameLoginException extends RuntimeException {
    public SameLoginException(String errorMessage) {
        super(errorMessage);
    }
}
