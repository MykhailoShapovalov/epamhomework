package NoteBook2.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {

    private final String REGEX_NAME_UKR = "^[А-ЩЬЮЯҐІЇЄ][а-щьюяґіїє']{1,20}$";
    private final String REGEX_NAME_LAT = "^[A-Z][a-z]{1,20}$";
    private final String REGEX_LOGIN = "^[A-Za-z0-9_-]{3,20}$";
    private final String SAME_LOGIN_EXCEPTION = "This login isn`t available!";

    private final Pattern patternNameLat = Pattern.compile(REGEX_NAME_LAT);
    private final Pattern patternNameUkr = Pattern.compile(REGEX_NAME_UKR);
    private final Pattern patternLogin = Pattern.compile(REGEX_LOGIN);

    public String checkNameLat(String nameLat) {
        Matcher matcher = patternNameLat.matcher(nameLat);
        if (matcher.matches()) {
            return nameLat;
        }
        return null;
    }

    public String checkNameUkr(String nameUkr) {
        Matcher matcher = patternNameUkr.matcher(nameUkr);
        if (matcher.matches()) {
            return nameUkr;
        }
        return null;
    }

    public String checkLogin(String login) {
        Matcher matcher = patternLogin.matcher(login);
        if (matcher.matches()) {
            return login;
        }
        return null;
    }

    public void checkSameLogin(String login) {
        if (NoteBook.checkLogin(login)) throw new SameLoginException(SAME_LOGIN_EXCEPTION);
    }

}
