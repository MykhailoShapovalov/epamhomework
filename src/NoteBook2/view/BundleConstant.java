package NoteBook2.view;

public enum BundleConstant {
    WELCOME("input.string.welcome"),
    CHANGE_LANGUAGE("input.string.changeLanguage"),
    WRITE_NAME("input.string.writeName"),
    WRITE_NICKNAME("input.string.writeNickname"),
    WRONG_NAME("input.string.wrongName"),
    WRONG_NICKNAME("input.string.wrongNickname"),
    SUCCESS("input.string.success");

    public String command;

    BundleConstant(String command) {
        this.command = command;
    }
}
