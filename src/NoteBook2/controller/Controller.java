package NoteBook2.controller;

import NoteBook2.model.Model;
import NoteBook2.model.SameLoginException;
import NoteBook2.view.View;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import NoteBook2.view.BundleConstant;

public class Controller {
    Model model;
    View view;
    Scanner scanner;
    private final String BUNDLE_NAME = "NoteBook2.message";
    private final String YES = "yes";
    private final String YES_UA = "так";

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void start() {
        scanner = new Scanner(System.in);
        boolean stop = false;
        ResourceBundle bundle = null;
        while (!stop) {
            bundle = ResourceBundle.getBundle(BUNDLE_NAME,
                    new Locale("ua", "UA"));
            view.showInfo(bundle.getString(BundleConstant.WELCOME.command));
            view.showInfo(bundle.getString(BundleConstant.CHANGE_LANGUAGE.command));
            if (scanner.nextLine().equals(YES_UA)) {
                bundle = ResourceBundle.getBundle(BUNDLE_NAME,
                        new Locale("en", "EN"));
                view.showInfo(bundle.getString(BundleConstant.WELCOME.command));
                view.showInfo(bundle.getString(BundleConstant.CHANGE_LANGUAGE.command));
                if (scanner.nextLine().equals(YES)) ;
                else {
                    stop = true;
                }
            } else {
                stop = true;
            }
        }
        view.showInfo(bundle.getString(BundleConstant.WRITE_NAME.command));
        String name = scanner.nextLine();
        if (bundle.getLocale().getLanguage().equals("ua")) {
            while (model.checkNameUkr(name) == null) {
                view.showInfo(bundle.getString(BundleConstant.WRONG_NAME.command));
                view.showInfo(bundle.getString(BundleConstant.WRITE_NAME.command));
                name = scanner.nextLine();
            }
        } else {
            while (model.checkNameLat(name) == null) {
                view.showInfo(bundle.getString(BundleConstant.WRONG_NAME.command));
                view.showInfo(bundle.getString(BundleConstant.WRITE_NAME.command));
                name = scanner.nextLine();
            }
        }
        view.showInfo(bundle.getString(BundleConstant.WRITE_NICKNAME.command));
        String login = scanner.nextLine();
        while (model.checkLogin(login) == null) {
            view.showInfo(bundle.getString(BundleConstant.WRONG_NICKNAME.command));
            view.showInfo(bundle.getString(BundleConstant.WRITE_NICKNAME.command));
            login = scanner.nextLine();
        }
        boolean checkSame = true;
        while (checkSame) {
            try {
                model.checkSameLogin(login);
                checkSame = false;
                view.showInfo(bundle.getString(BundleConstant.SUCCESS.command));
            } catch (SameLoginException e) {
                System.err.println(e);
                view.showInfo(bundle.getString(BundleConstant.WRITE_NICKNAME.command));
                login = scanner.nextLine();
            }
        }

    }

}
